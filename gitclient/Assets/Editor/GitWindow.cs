﻿using UnityEngine;
using System.Collections;
using UnityEditor;
using System.Linq;
using System.Diagnostics;
using System.Collections.Generic;

public class GitWindow : EditorWindow
{
    string GIT_PATH = "C:\\Program Files (x86)\\Git\\bin\\git.exe";
    string GIT_WD = "C:\\Users\\Daniel\\Documents\\Unity Projects\\boringtestrepo";

    Vector2 m_scrollPos;
    string m_commitMessage = "";

    class ChangeEntry
    {
        public bool isChecked;
        public string file;
        public string type;
    }

    List<ChangeEntry> m_entries = new List<ChangeEntry>();

    [MenuItem("Test/My Window")]
    static void Init()
    {
        GitWindow window = (GitWindow)EditorWindow.GetWindow(typeof(GitWindow));
        window.Show();
    }

    string GitStatus()
    {
        ProcessStartInfo info = new ProcessStartInfo();
        info.FileName = GIT_PATH;
        info.Arguments = "status --porcelain -u";
        info.WorkingDirectory = GIT_WD;
        info.RedirectStandardOutput = true;
        info.UseShellExecute = false;
        info.WindowStyle = ProcessWindowStyle.Minimized;

        var process = Process.Start(info);
        var stdout = process.StandardOutput.ReadToEnd();

        process.WaitForExit();

        return stdout;
    }

    string GitStashSave()
    {
        ProcessStartInfo info = new ProcessStartInfo();
        info.FileName = GIT_PATH;
        info.Arguments = "stash save -u";
        info.WorkingDirectory = GIT_WD;
        info.RedirectStandardOutput = true;
        info.UseShellExecute = false;
        info.WindowStyle = ProcessWindowStyle.Minimized;

        var process = Process.Start(info);
        var stdout = process.StandardOutput.ReadToEnd();

        process.WaitForExit();

        return stdout;
    }

    string GitStashPop()
    {
        ProcessStartInfo info = new ProcessStartInfo();
        info.FileName = GIT_PATH;
        info.Arguments = "stash pop";
        info.WorkingDirectory = GIT_WD;
        info.RedirectStandardOutput = true;
        info.UseShellExecute = false;
        info.WindowStyle = ProcessWindowStyle.Minimized;

        var process = Process.Start(info);
        var stdout = process.StandardOutput.ReadToEnd();

        process.WaitForExit();

        return stdout;
    }

    string GitPull()
    {
        ProcessStartInfo info = new ProcessStartInfo();
        info.FileName = GIT_PATH;
        info.Arguments = "pull";
        info.WorkingDirectory = GIT_WD;
        info.RedirectStandardOutput = true;
        info.UseShellExecute = false;
        info.WindowStyle = ProcessWindowStyle.Minimized;

        var process = Process.Start(info);
        var stdout = process.StandardOutput.ReadToEnd();

        process.WaitForExit();

        return stdout;
    }


    void OnGUI()
    {
        m_scrollPos = GUILayout.BeginScrollView(m_scrollPos);

        GUILayout.BeginHorizontal();

        if (GUILayout.Button("Status"))
        {
            string stdout = GitStatus();
            UnityEngine.Debug.Log(stdout);

            var changes = stdout.Split('\n');
            foreach (var change in changes)
            {
                if (string.IsNullOrEmpty(change))
                    continue;

                var stuff = change.Split(' ');
                string type = stuff[0];
                string file = stuff[1];

                var entry = m_entries.FirstOrDefault(e => { return e.file == file; });
                if (entry != null)
                {
                    entry.type = type;
                }
                else
                {
                    ChangeEntry newEntry = new ChangeEntry();
                    newEntry.file = file;
                    newEntry.type = type;
                    newEntry.isChecked = true;
                    m_entries.Add(newEntry);
                }
            }
        }

        if (GUILayout.Button("Update"))
        {
            bool shouldStash = m_entries.Count != 0;
            string stdout = "";

            if (shouldStash)
            {
                stdout = GitStashSave();
                UnityEngine.Debug.Log(stdout);
            }

            stdout = GitPull();
            UnityEngine.Debug.Log(stdout);

            if (shouldStash)
            {
                stdout = GitStashPop();
                UnityEngine.Debug.Log(stdout);
            }
        }


        if (GUILayout.Button("Push"))
        {

        }

        GUILayout.EndHorizontal();

        m_commitMessage = GUILayout.TextArea(m_commitMessage);

        foreach (var e in m_entries)
        {
            GUILayout.BeginHorizontal();
            e.isChecked = GUILayout.Toggle(e.isChecked, e.type + "  -  " + e.file);
            GUILayout.EndHorizontal();
        }

        GUILayout.EndScrollView();
    }
}
